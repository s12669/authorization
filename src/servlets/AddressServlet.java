package servlets;

import storage.AddressStorage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/address")
public class AddressServlet extends HttpServlet {
    private AddressStorage addressStorage;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestType = req.getParameter("type");
        String requestProvince = req.getParameter("province");
        String requestCity = req.getParameter("city");
        String requestZip = req.getParameter("zip");
        String requestStreet = req.getParameter("street");
        String requestNumber = req.getParameter("number");

        this.addressStorage.put(requestType, requestProvince, requestCity, requestZip, requestStreet, requestNumber);
        req.getRequestDispatcher("profile.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("addresses", addressStorage.getAll());
        req.getRequestDispatcher("/address.jsp").forward(req, resp);
    }

    @Override
    public void init() throws ServletException {
        this.addressStorage = AddressStorage.getInstance();
    }
}