package servlets;

import storage.AddressStorage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {
    private AddressStorage addressStorage;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("addresses", addressStorage.getAll());
        req.getRequestDispatcher("/data.jsp").forward(req, resp);
    }
    @Override
    public void init() throws ServletException {
        this.addressStorage = AddressStorage.getInstance();
    }
}
