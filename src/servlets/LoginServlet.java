package servlets;

import storage.UserStorage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private UserStorage userStorage;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestLogin = req.getParameter("login");
        String requestPassword = req.getParameter("password");

        if (!userStorage.get(requestLogin).getPassword().equals(requestPassword)) {
            resp.sendRedirect("/loginfail.jsp");
            return;
        }

        req.getSession().setAttribute("logged_in", true);
        Cookie loginCookie = new Cookie("login", requestLogin);
        loginCookie.setMaxAge(2 * 60);
        resp.addCookie(loginCookie);
        resp.sendRedirect("/profile");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    @Override
    public void init() throws ServletException {
        this.userStorage = UserStorage.getInstance();
    }
}
