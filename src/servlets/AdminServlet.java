package servlets;

import storage.UserStorage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/admin")
public class AdminServlet extends HttpServlet {
    private UserStorage userStorage;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestLogin = req.getParameter("login");

        if (!userStorage.exists(requestLogin)) {
            resp.sendRedirect("/adminfail.jsp");
            return;
        }
        if (!requestLogin.equals("admin")) {
            userStorage.get(requestLogin).setRole("premium");
            resp.sendRedirect("/admin");
        }
        else resp.sendRedirect("/adminfail2.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("users", userStorage.getAll());
        req.getRequestDispatcher("/admin.jsp").forward(req, resp);
    }

    @Override
    public void init() throws ServletException {
        this.userStorage = UserStorage.getInstance();
    }
}
