package servlets;

import storage.UserStorage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {
    private UserStorage userStorage;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestLogin = req.getParameter("login");
        String requestPassword = req.getParameter("password");
        String requestConfirm = req.getParameter("confirm");

        if (!requestPassword.equals(requestConfirm)) {
            resp.sendRedirect("/registerfail.jsp");
            return;
        }

        this.userStorage.put(requestLogin, requestPassword);
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/register.jsp").forward(req, resp);
    }

    @Override
    public void init() throws ServletException {
        this.userStorage = UserStorage.getInstance();
    }
}
