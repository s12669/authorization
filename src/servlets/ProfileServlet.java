package servlets;

import cookie.CookieGetter;
import storage.UserStorage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/profile")
public class ProfileServlet extends HttpServlet {
    private UserStorage userStorage;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userLogin = CookieGetter.get(req, resp, "login");
        req.getSession().setAttribute("User_Login", userLogin);
        req.getSession().setAttribute("User_Role", userStorage.get(userLogin).getRole());
        req.getRequestDispatcher("/profile.jsp").forward(req, resp);
    }

    @Override
    public void init() throws ServletException {
        this.userStorage = UserStorage.getInstance();
    }
}
