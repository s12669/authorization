package servlets;

import storage.AddressStorage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/delete")
public class DeleteServlet extends HttpServlet {
    private AddressStorage addressStorage;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestType = req.getParameter("type");

        this.addressStorage.delete(requestType);
        req.getRequestDispatcher("profile.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("addresses", addressStorage.getAll());
        req.getRequestDispatcher("/delete.jsp").forward(req, resp);
    }

    @Override
    public void init() throws ServletException {
        this.addressStorage = AddressStorage.getInstance();
    }
}
