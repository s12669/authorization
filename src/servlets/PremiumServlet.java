package servlets;

import cookie.CookieGetter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/premium")
public class PremiumServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userLogin = CookieGetter.get(req, resp, "login");
        req.getSession().setAttribute("User_Login", userLogin);
        req.getRequestDispatcher("/premium.jsp").forward(req, resp);
    }
}
