package filters;

import storage.UserStorage;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/premium")
public class PremiumFilter implements Filter {
    private UserStorage userStorage;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.userStorage = UserStorage.getInstance();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        String userLogin = "";

        Cookie[] cookies = req.getCookies();
        if (cookies == null) {
            resp.sendRedirect("/login");
            return;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                userLogin = cookie.getValue();
            }
        }

        if (!userStorage.exists(userLogin)) {
            resp.sendRedirect("/login");
            return;
        }

        if (!userStorage.get(userLogin).getRole().equals("premium") &&
                !userStorage.get(userLogin).getRole().equals("admin")) {
            resp.sendRedirect("/profile");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
