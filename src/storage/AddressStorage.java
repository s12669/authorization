package storage;

import domain.Address;

import java.util.ArrayList;
import java.util.HashMap;

public class AddressStorage {
    private static AddressStorage storeInstance;
    private HashMap<String, Address> addressStorage;

    private AddressStorage() {
        storeInstance = null;
        addressStorage = new HashMap<>();
    }

    public static AddressStorage getInstance() {
        if (storeInstance == null) {
            storeInstance = new AddressStorage();
        }

        return storeInstance;
    }

    public Boolean exists(String username) {
        return this.addressStorage.containsKey(username);
    }

    public void put(String type, String province, String city, String zipCode, String street, String houseNumber) {
       Address newAddress = new Address(type, province, city, zipCode, street, houseNumber);

        this.addressStorage.put(type, newAddress);
    }

    public void delete(String type){
        this.addressStorage.remove(type);
    }
    public Address get(String type) {
        return this.addressStorage.get(type);
    }

    public ArrayList<Address> getAll() {
        return new ArrayList<>(this.addressStorage.values());
    }
}
