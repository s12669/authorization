package storage;

import domain.User;

import java.util.ArrayList;
import java.util.HashMap;

public class UserStorage {
    private static UserStorage storeInstance;
    private HashMap<String, User> userStorage;

    private UserStorage() {
        storeInstance = null;
        userStorage = new HashMap<>();
        User admin = new User("admin","admin");
        admin.setRole("admin");
        this.userStorage.put("admin", admin);
    }

    public static UserStorage getInstance() {
        if (storeInstance == null) {
            storeInstance = new UserStorage();
        }

        return storeInstance;
    }

    public Boolean exists(String username) {
        return this.userStorage.containsKey(username);
    }

    public void put(String username, String password) {
        User newUser = new User(username, password);

        if (this.userStorage.isEmpty()) {
            newUser.setRole("admin");
        }

        this.userStorage.put(username, newUser);
    }

    public User get(String username) {
        return this.userStorage.get(username);
    }

    public ArrayList<User> getAll() {
        return new ArrayList<>(this.userStorage.values());
    }
}
