package cookie;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CookieGetter {
    public static String get(HttpServletRequest req, HttpServletResponse resp, String name) throws ServletException, IOException {
        String value = "";

        Cookie[] cookies = req.getCookies();
        if (cookies == null) {
            req.getRequestDispatcher("login.jsp").forward(req, resp);
            return value;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                value = cookie.getValue();
            }
        }

        return value;
    }
}
