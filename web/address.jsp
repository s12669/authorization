<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Address</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/address" method="post">
    <div class="form-group">
        <label for="type">Type</label>
        <select name="type" id = "type">
            <option value="permanent">permanent address</option>
            <option value="correspondence">correspondence address</option>
            <option value="job">job address</option>
        </select>
    </div>

    <div class="form-group">
        <label for="province">Province</label>
        <select name="province" id = "province">
            <option value="dolnoslaskie">dolnośląskie</option>
            <option value="kujawsko-pomorskie">kujawsko-pomorskie</option>
            <option value="lubelskie">lubelskie</option>
            <option value="lubuskie">lubuskie</option>
            <option value="lodzkie">łódzkie </option>
            <option value="malopolskie">małopolskie </option>
            <option value="mazowieckie">mazowieckie </option>
            <option value="opolskie">opolskie </option>
            <option value="podkarpackie">podkarpackie </option>
            <option value="podlaskie">podlaskie </option>
            <option value="pomorskie">pomorskie </option>
            <option value="slaskie">śląskie </option>
            <option value="swietokrzyskie">świętokrzyskie </option>
            <option value="warminsko-mazurskie">warmińsko-mazurskie </option>
            <option value="wielkopolskie">wielkopolskie </option>
            <option value="zachodniopomorskie">zachodniopomorskie </option>
        </select>
    </div>

    <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control" id="city" name="city">
    </div>

    <div class="form-group">
        <label for="zip">Zip-Code</label>
        <input type="text" class="form-control" id="zip" name="zip">
    </div>

    <div class="form-group">
        <label for="street">Street</label>
        <input type="text" class="form-control" id="street" name="street">
    </div>

    <div class="form-group">
        <label for="number">House Number</label>
        <input type="text" class="form-control" id="number" name="number">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
</body>
</html>
