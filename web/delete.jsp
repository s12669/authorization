<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/delete" method="post">
    <div class="form-group">
        <label for="type">Type</label>
        <select name="type" id = "type">
            <option value="permanent">permanent address</option>
            <option value="correspondence">correspondence address</option>
            <option value="job">job address</option>
        </select>
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
</body>
</html>
