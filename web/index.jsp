<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Homepage</title>
  </head>
  <body>
  <h1>Authorization system homepage</h1>
  <a href="/register">Register new participant</a><br />
  <a href="/login">Log in</a><br />
  <a href="/profile">Profile</a>
  </body>
</html>
