<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration Fail</title>
</head>
<body>
<h1>Registration failed. Cause: passwords do not match.</h1>
<a href="/register.jsp">Try again</a><br />
<a href="/index.jsp">Go back to homepage</a><br />
</body>
</html>
