<%@ page import="domain.Address" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Address Data</title>
</head>
<body>
<% ArrayList<Address> addresses = (ArrayList<Address>) request.getAttribute("addresses"); %>
<h5>Your Address Data</h5>

<style scoped>td { padding: 8px;}</style>
<table>
    <tr>
        <th>Type</th>
        <th>Province</th>
        <th>City</th>
        <th>ZipCode</th>
        <th>Street</th>
        <th>House Number</th>
    <tr>
    <% for (Address address: addresses) { %>
    <tr>
        <td><%= address.getType() %></td>
        <td><%= address.getProvince() %></td>
        <td><%= address.getCity() %></td>
        <td><%= address.getZipCode() %></td>
        <td><%= address.getStreet() %></td>
        <td><%= address.getHouseNumber()+"<br />" %></td>
    </tr>
    <% } %>
</table>
<a href="${pageContext.request.contextPath}/profile.jsp">Go back to your profile</a>
</body>
</html>
