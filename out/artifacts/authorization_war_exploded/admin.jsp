<%@ page import="domain.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Admin page - add Premium</h1>
<% ArrayList<User> users = (ArrayList<User>) request.getAttribute("users"); %>
<td>
    <h5>List of users</h5>
    <tr>User</tr>
    <tr>Role<br /><br /></tr>
<% for (User user: users) { %>

    <tr><%= user.getLogin() %></tr>
    <tr><%= user.getRole()+"<br />" %></tr>
</td>
<% } %>

<form action="${pageContext.request.contextPath}/admin" method="post">
    <div class="form-group">
        <label for="login">Give Premium to:</label>
        <input type="text" class="form-control" id="login" name="login">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
<a href="${pageContext.request.contextPath}/profile.jsp">Go back to your profile</a>
</body>
</html>
