<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logged Out</title>
</head>
<body>
<h1>You are not logged in</h1>
<a href="/login.jsp">Log in</a><br />
<a href="/index.jsp">Go back to homepage</a><br />
</body>
</html>
