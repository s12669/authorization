<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/login" method="post">
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login">
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input type="text" class="form-control" id="password" name="password">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
</body>
</html>