<%--@elvariable id="User_Login" type="domain.User.login"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<h1>Hello ${ User_Login }</h1>
<h4>This is your profile page.</h4>
<a href="${pageContext.request.contextPath}/address">Add/modify address data</a>
<a href="${pageContext.request.contextPath}/data">View your address data</a>
<a href="${pageContext.request.contextPath}/delete">Delete your address data</a>
<% if (session.getAttribute("User_Role").equals("premium")) { %>
<a href="${pageContext.request.contextPath}/premium">Premium</a>
<% } %>

<% if (session.getAttribute("User_Role").equals("admin")) { %>
<a href="${pageContext.request.contextPath}/premium">Premium</a>
<a href="${pageContext.request.contextPath}/admin">Admin</a>
<% } %>
</body>
</html>
